# Quotebot quote picker

Quote picker for epilogues in project descriptions, READMEs, etc.

## Operation

In typical use, it picks a pseudo-random quote from a store of JSON-serialized
with the utility, in `/etc/quotebot.json`, in `$HOME/.config/quotebot.json`,
or in a specified location.

A base set of quotes is included in the package at `${approot}/etc/base.json`.

## Example JSON format

```json
{
  "quotes": [
    {
      "author": "Karl Marx",
      "source": "Critique of the Gotha Program",
      "date": "1875-05",
      "paraphrased": {
        "by": "SMD",
        "reason": "modern pronouns"
      },
      "text": "From each according to their ability, to each according to their needs"
    }
  ]
}
```

>_I am, somehow, less interested in the weight and convolutions of Einstein’s brain than in the near certainty that people of equal talent have lived and died in cotton fields and sweatshops._
